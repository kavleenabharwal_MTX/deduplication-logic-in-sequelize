    // this is a deduplication logic on fields - first name, last name, phone number and suffix for records stored in jsonb format in postgres
    //this logic also works if existing record has a field among the above 4 and the new incoming record does not
    
    let existingTravelerQuerySnapshot = await travellerTable.findAll({ 
                                 where: {
                                    [Op.or]: [{
                                        'data.Perm_Cell_Pager' : req.body.Perm_Cell_Pager,
                                        'data.First_Name' : req.body.First_Name,
                                        'data.Last_Name' : req.body.Last_Name,
                                        'data.DOB' : req.body.DOB,
                                        'data.Suffix' : req.body.Suffix
                                    }, 
                                    {
                                        'data.Perm_Cell_Pager' : req.body.Perm_Cell_Pager,
                                      'data.First_Name' : req.body.First_Name,
                                      'data.Last_Name' : req.body.Last_Name,
                                      'data.Suffix' : req.body.Suffix
                                    },
                                    {
                                        'data.Perm_Cell_Pager' : req.body.Perm_Cell_Pager,
                                        'data.First_Name' : req.body.First_Name,
                                        'data.Last_Name' : req.body.Last_Name,
                                        'data.DOB' : req.body.DOB,
                                    },
                                    {
                                        'data.Perm_Cell_Pager' : req.body.Perm_Cell_Pager,
                                        'data.First_Name' : req.body.First_Name,
                                        'data.Last_Name' : req.body.Last_Name,
                                    }]

                                } });
                                
                                //if exisiting duplicate record is found
                                     if (existingTravelerQuerySnapshot.length > 0) {
                                travelerDocRef = existingTravelerQuerySnapshot[0];
                                let existingTraveler = travelerDocRef.data;
                                let isJustDateNotSame = false;
                                
                                // comparing 2 DOBs by ignoring the time and only comparing the date part of ISO string
                            
                                if(req.body.DOB && existingTraveler.DOB){
                                let reqJustDateArr = req.body.DOB.split("T");
                                let exisitingJustDateArr = existingTraveler.DOB.split("T");
                                isJustDateNotSame = true;
                                if (reqJustDateArr.length > 0 && exisitingJustDateArr.length > 0) {
                                    if (reqJustDateArr[0] == exisitingJustDateArr[0]) {
                                        isJustDateNotSame = false;
                                    }
                                }
                            }
                                if ((req.body.DOB && existingTraveler.DOB && req.body.DOB !== existingTraveler.DOB && isJustDateNotSame)) {
                                    travelerDocRef = null;
                                } 

                                
                               else if(existingTraveler.Suffix && req.body.Suffix){
                                    if(existingTraveler.Suffix!=req.body.Suffix){
                                        travelerDocRef = null;
                                    }
                                } 
                                else {
                                // when existing is case and try to enter contact throw error
                                //on basis on business logic the below conditions apply
                                if (existingTraveler.Type != req.body.Type && existingTraveler.Type == 'Case' &&  req.body.Type == 'Contact') {
                                    let err = '';
                                    if (shouldReturnData) {
                                        return {
                                            error: "User already exists in the system"
                                        };
                                    } else {
                                        return res.status(400).send({
                                            data: {},
                                            error: "User already exists in the system"
                                        });
                                    }
                                }
                                else if (existingTraveler.Type != req.body.Type && existingTraveler.Source_Category == 'Self Reported') {
                                   //bases on project logic a new traveler/contact is added
                                   // allowing traveller to be added if the exisiting user is Citizen
                                    travelerDocRef = null;
                                }
                                else if(existingTraveler.Type != req.body.Type && existingTraveler.Type == 'Contact' &&  req.body.Type == 'Case'){
                                    req.body.Disposition = 'Monitoring Stopped - Case Reported';
                                     req.body.Type= 'Case';
                                     req.body.Monitoring_ID = existingTraveler.Monitoring_ID;
                                     req.body.Contact_Type = 'Completed';
                                     req.body.Dialer_Ready_Status = 'Completed';
                                     
                               }
                                else{
                                    req.body.Monitoring_ID = existingTraveler.Monitoring_ID;
                                }    
                            }
                            }